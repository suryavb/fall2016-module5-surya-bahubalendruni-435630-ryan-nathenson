<?php
header("Content-Type: application/json");

if(isset($_POST['username']) and isset($_POST['user_type']) and isset($_POST['password'])){

	$username = (string) $_POST['username'];
	$password = (string) $_POST['password'];
	$user_type = (string) $_POST['user_type'];
	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');

	if($mysqli->connect_errno) {
				echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Connection Failed"
				));
				exit;
	}
	// New user
	if($user_type == "new"){
			$usernamequery = $mysqli->prepare("select username from users");
			if(!$usernamequery){
				echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Query Prep Failed"
				));
				exit;
			}

			$usernamequery->execute();
			$usernamequeryresult = $usernamequery->get_result();
			while($row = $usernamequeryresult->fetch_assoc()){
				if ($username == $row['username']){
					echo json_encode(array(
					"success" => false,
					"message" => "You were not logged in. Error: Username is taken"
					));
					exit;
				}
			}
			$usernameinsert = $mysqli->prepare("insert into users (username, password) values (?, ?)");
			if(!$usernameinsert){
				echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Query Prep Failed"
				));
				exit;
			}
			$usernameinsert-> bind_param('ss', $username, crypt($password));
			$usernameinsert->execute();
			$usernameinsert->close();
			ini_set("session.cookie_httponly", 1);
			session_start();
			$_SESSION['username'] = $username;
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
			echo json_encode(array(
				"success" => true,
				"message" => "New user has logged in",
				"hidden_token" => $_SESSION['token']
				));
				exit;
			}

	// existing user
	else{
		$usernamequery = $mysqli->prepare("select username, password from users where (username) like (?)");
		if(!$usernamequery){
				echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Query Prep Failed"
				));
				exit;
		}
		$usernamequery -> bind_param('s', $username);
		$usernamequery-> execute();
		$usernamequeryresult = $usernamequery->get_result();
		$row = $usernamequeryresult->fetch_assoc();
		if(is_null($row)){
			echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Username does not exist!"
			));
			exit;
		}

		if($username == $row['username'] && crypt($password, $row['password']) == $row['password']){
			ini_set("session.cookie_httponly", 1);
			session_start();
			$_SESSION['username'] = $username;
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
			echo json_encode(array(
				"success" => true,
				"message" => "Existing user has logged in",
				"hidden_token" => $_SESSION['token']
			));
			exit;
		}

		else{
			echo json_encode(array(
				"success" => false,
				"message" => "You were not logged in. Error: Incorrect Username or Password"
			));
			exit;
		}
	}
}
?>