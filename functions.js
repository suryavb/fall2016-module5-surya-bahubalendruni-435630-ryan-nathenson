document.getElementById("submit").addEventListener("click", loginAjax, false);

// login information
function loginAjax(event){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	var user_type = document.getElementsByName("user_type")[0].checked ? document.getElementsByName("user_type")[0].value : document.getElementsByName("user_type")[1].value;
	var uri_string = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + "&user_type=" + encodeURIComponent(user_type);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "register_login.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var login_json = JSON.parse(event.target.responseText);
		alert(login_json.message);
		if (login_json.success){
			populateAddForms(login_json);
			deleteUser(login_json);
			populateDelForms(login_json);
			populateShare(login_json);
			viewSpecificEvent(login_json);
			viewEvents();
			// document.getElementById("del_submit").addEventListener("click", delAjax, false);
			// populateModifyForms(login_json);
			// document.getElementById("mod_submit").addEventListener("click", modAjax, false);
		}
	}, false);
	xmlHttp.send(uri_string);
}


// calendar view creation
var currentMonth = new Month(2016, 9);
updateCalendarDates();
document.getElementById("next_month").addEventListener("click", function(event){
	currentMonth = currentMonth.nextMonth();
	updateCalendarDates();
}, false);

document.getElementById("prev_month").addEventListener("click", function(event){
	currentMonth = currentMonth.prevMonth();
	updateCalendarDates();
}, false);

function updateCalendarDates(){
	var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	var day_names = new Array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
	document.getElementById("date_title").innerHTML = month_names[currentMonth.month] + " " + currentMonth.year;
	document.getElementById("prev_month").innerHTML = "&larr; " + month_names[currentMonth.prevMonth().month];
	document.getElementById("next_month").innerHTML = month_names[currentMonth.nextMonth().month] + " &rarr;";

	var weeks = currentMonth.getWeeks();

	for (var i = 0; i < 6; i++){
		for (var j = 0; j < day_names.length; j++){
			document.getElementById("calendar_table").getElementsByClassName("week"+i)[0].getElementsByClassName(day_names[j])[0].innerHTML = "";
		}
	}


	for(var w = 0; w < weeks.length; w++){
		var days = weeks[w].getDates();
		for(var d = 0; d < days.length; d++){
			if (days[d].getMonth() == currentMonth.month){
				var display_string = days[d].toISOString().split("T")[0].split("-")[2];
				document.getElementById("calendar_table").getElementsByClassName("week"+w)[0].getElementsByClassName(day_names[d])[0].innerHTML = display_string;
			}
		}
	}
}



// calendar add form creation
function populateAddForms(login_json){
	var months = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

	var add_form = document.createElement("form");
	add_form.setAttribute("id", "add_form");
	add_form.appendChild(document.createElement("br"));
	add_form.appendChild(document.createElement("br"));
	add_form.appendChild(document.createTextNode("ADD EVENT: "));

	// input event name section
	var add_name_input = document.createElement("input");
	add_name_input.setAttribute("type", "text");
	add_name_input.setAttribute("id", "add_name_input");
	add_name_input.setAttribute("placeholder", "Event Name");

	// input event description section
	var add_desc_input = document.createElement("input");
	add_desc_input.setAttribute("type", "block");
	add_desc_input.setAttribute("id", "add_desc_input");
	add_desc_input.setAttribute("placeholder", "Description");

	// input month section
	var add_month_input = document.createElement("select");
	add_month_input.setAttribute("id", "add_month_input");
	for(var a = 0; a < months.length; a++){
		var option = document.createElement("option");
		option.setAttribute("value", months[a]);
		option.text = months[a];
		add_month_input.appendChild(option);
	}

	// input day section is disabled at the start
	var add_day_input = document.createElement("select");
	add_day_input.setAttribute("id", "add_day_input");
	add_day_input.disabled = true;

	var add_year_input = document.createElement("input");
	add_year_input.setAttribute("type", "text");
	add_year_input.setAttribute("id", "add_year_input");
	//year_input.setAttribute("placeholder", "Year");
	add_year_input.defaultValue = String(currentMonth.year);

	var add_submit = document.createElement("input");
	add_submit.setAttribute("type", "button");
	add_submit.setAttribute("id", "add_submit");
	add_submit.setAttribute("value", "Add Event");

	var add_hour_input = document.createElement("select");
	add_hour_input.setAttribute("id", "add_hour_input");
	for(var b = 0; b < 24; b++){
		var hour_option = document.createElement("option");
		hour_option.setAttribute("Value", b);
		hour_option.text = b;
		add_hour_input.appendChild(hour_option);
	}

	var add_minute_input = document.createElement("select");
	add_minute_input.setAttribute("id", "add_minute_input");
	for(var c = 0; c < 60; c++){
		var minute_option = document.createElement("option");
		minute_option.setAttribute("value", c);
		minute_option.text = c;
		add_minute_input.appendChild(minute_option);
	}

	var categories_input = document.createElement("select");
	categories_input.setAttribute("id", "add_categories_input");
	var work_option = document.createElement("option");
	work_option.setAttribute("value", "Work");
	work_option.text = "Work";
	categories_input.appendChild(work_option);
	var school_option = document.createElement("option");
	school_option.setAttribute("value", "School");
	school_option.text = "School";
	categories_input.appendChild(school_option);
	var tasks_option = document.createElement("option");
	tasks_option.setAttribute("value", "Tasks");
	tasks_option.text = "Tasks";
	categories_input.appendChild(tasks_option);
	var meetings_option = document.createElement("option");
	meetings_option.setAttribute("value", "Meetings");
	meetings_option.text = "Meetings";
	categories_input.appendChild(meetings_option);


	var add_token = document.createElement("input");
	add_token.setAttribute("type", "hidden");
	add_token.setAttribute("id", "add_token");
	add_token.setAttribute("value", String(login_json.hidden_token));
	// ADD A FUNCTION TO THE BUTTON (SEND INFORMATION OF THE FORM TO PHP FILE)

	// populates div with initial add form
	add_form.appendChild(add_name_input);
	add_form.appendChild(add_desc_input);
	add_form.appendChild(categories_input);
	add_form.appendChild(document.createTextNode("   Time:"));
	add_form.appendChild(add_hour_input);
	add_form.appendChild(add_minute_input);
	add_form.appendChild(document.createTextNode("   Date:"));
	add_form.appendChild(add_year_input);
	add_form.appendChild(add_month_input);
	add_form.appendChild(add_day_input);
	add_form.appendChild(add_token);

	document.getElementById("calendar_manage").appendChild(add_form);

	// creating a listener that waits for a month to be selected before enabling and populating day_input with options
	document.getElementById("add_month_input").addEventListener("change", function(event){
		document.getElementById("add_day_input").remove();
		var add_day_input = document.createElement("select");
		add_day_input.setAttribute("id", "add_day_input");
		add_day_input.disabled = true;
		document.getElementById("add_form").appendChild(add_day_input);
		var add_month = new Month(2016, months.indexOf(document.getElementById("add_month_input").value));
		var add_weeks = add_month.getWeeks();
		var add_days_in_month = 0;
		for (var ix = 0; ix < add_weeks.length; ix++){
			var add_days = add_weeks[ix].getDates();
			for(var dx = 0; dx < add_days.length; dx++){
				if (add_days[dx].getMonth() == add_month.month){
					add_days_in_month += 1;
					var day_option = document.createElement("option");
					day_option.setAttribute("value", add_days_in_month);
					day_option.text = add_days_in_month;
					add_day_input.appendChild(day_option);
					//alert(add_days[dx].toISOString() + "  " + add_days_in_month);
				}
			}
		}
		add_day_input.disabled = false;
		add_form.appendChild(add_submit);
		document.getElementById("add_submit").addEventListener("click", addAjax, false);
	}, false);
}

// function for sending new event data to be added by add_event.php
function addAjax(event){
	var add_event_name = document.getElementById("add_name_input").value;
	var add_event_description = document.getElementById("add_desc_input").value;
	var add_event_category = String(document.getElementById("add_categories_input").selectedIndex);
	var add_event_month = String(document.getElementById("add_month_input").selectedIndex);
	var add_event_year = document.getElementById("add_year_input").value;
	var add_event_day = String(document.getElementById("add_day_input").selectedIndex + 1);
	var add_event_hour = String(document.getElementById("add_hour_input").selectedIndex);
	var add_event_minute = String(document.getElementById("add_minute_input").selectedIndex);
	var add_token = document.getElementById("add_token").value;

	var uri_string = "add_event_name=" + encodeURIComponent(add_event_name) + "&add_event_description=" + encodeURIComponent(add_event_description) + "&add_event_month=" + encodeURIComponent(add_event_month) + "&add_event_year=" + encodeURIComponent(add_event_year) +  "&add_event_day=" + encodeURIComponent(add_event_day) + "&add_event_hour=" + encodeURIComponent(add_event_hour) + "&add_event_minute=" + encodeURIComponent(add_event_minute) + "&add_token=" + encodeURIComponent(add_token) + "&add_event_category=" + encodeURIComponent(add_event_category);
	var add_http = new XMLHttpRequest();
	add_http.open("POST", "add_event.php", true);
	add_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	add_http.addEventListener("load", function(event){
		var add_event_json = JSON.parse(event.target.responseText);
		alert(add_event_json.message);
	}, false);
	add_http.send(uri_string);
	// viewEvents();
}

// creating the forms for deleting events, only shown after a user has logged in
function populateDelForms(login_json){
	// Create forms to delete events
	var del_form = document.createElement("form");
	del_form.setAttribute("id", "del_form");
	del_form.appendChild(document.createElement("br"));
	del_form.appendChild(document.createElement("br"));
	del_form.appendChild(document.createElement("br"));
	del_form.appendChild(document.createTextNode("DELETE EVENT: "));

	var del_event = document.createElement("input");
	del_event.setAttribute("type", "text");
	del_event.setAttribute("id", "del_event_id");
	del_event.setAttribute("placeholder", "Event ID");

	var del_submit = document.createElement("input");
	del_submit.setAttribute("type", "button");
	del_submit.setAttribute("id", "del_submit");
	del_submit.setAttribute("value", "Delete Event");

	var del_token = document.createElement("input");
	del_token.setAttribute("type", "hidden");
	del_token.setAttribute("id", "del_token");
	del_token.setAttribute("value", login_json.hidden_token);

	del_form.appendChild(del_event);
	del_form.appendChild(del_submit);
	del_form.appendChild(del_token);
	del_form.appendChild(document.createElement("br"));
	document.getElementById("calendar_manage").appendChild(del_form);
	document.getElementById("del_submit").addEventListener("click", deleteAjax, false);
	//viewEvents();
}

// function that sends information about deleting an event to the delete_event.php script
function deleteAjax(event){
	var raw_input = document.getElementById("del_event_id").value;
		if (isNaN(raw_input)){
			alert("Must input number for id");
		}
		else{
			var del_event_id = String(document.getElementById("del_event_id").value);
			var del_token = document.getElementById("del_token").value;
			var uri_string = "del_event_id=" + encodeURIComponent(del_event_id) +  "&del_token=" + encodeURIComponent(del_token);
			var del_http = new XMLHttpRequest();
			del_http.open("POST", "delete_event.php", true);
			del_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			del_http.addEventListener("load", function(event){
				var del_event_json = JSON.parse(event.target.responseText);
				alert(del_event_json.message);
			}, false);
			del_http.send(uri_string);
		}

}

// function populateModifyForms(){
// 		var mod_months = new Array(" ", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
// 		var mod_form = document.createElement("form");
// 		mod_form.setAttribute("id", "mod_form");
// 		mod_form.appendChild(document.createTextNode("Modify Event"));
// 		// modify event id
// 		var mod_id = document.createElement("input");
// 		mod_id.setAttribute("type", "text");
// 		mod_id.setAttribute("id", "mod_id");
// 		mod_id.setAttribute("placeholder", "Event ID");
// 		// modify event name section
// 		var mod_name_input = document.createElement("input");
// 		mod_name_input.setAttribute("type", "text");
// 		mod_name_input.setAttribute("id", "mod_name_input");
// 		mod_name_input.setAttribute("placeholder", "Event Name");
// 		// modify event description section
// 		var mod_desc_input = document.createElement("input");
// 		mod_desc_input.setAttribute("type", "block");
// 		mod_desc_input.setAttribute("id", "mod_desc_input");
// 		mod_desc_input.setAttribute("placeholder", "Description");
// 		// modify month section
// 		var mod_month_input = document.createElement("select");
// 		mod_month_input.setAttribute("id", "mod_month_input");
// 		for(var a = 0; a < mod_months.length; a++){
// 			var option = document.createElement("option");
// 			option.setAttribute("value", mod_months[a]);
// 			option.text = mod_months[a];
// 			mod_month_input.appendChild(option);
// 		}
// 		// modify day section is disabled at the start
// 		var mod_day_input = document.createElement("select");
// 		mod_day_input.setAttribute("id", "mod_day_input");
// 		mod_day_input.disabled = true;
// 		var mod_year_input = document.createElement("input");
// 		mod_year_input.setAttribute("type", "text");
// 		mod_year_input.setAttribute("id", "mod_year_input");
// 		mod_year_input.setAttribute("placeholder", "Year");
// 		var mod_submit = document.createElement("input");
// 		mod_submit.setAttribute("type", "button");
// 		mod_submit.setAttribute("id", "mod_submit");
// 		mod_submit.setAttribute("value", "Modify Event");

// 		var mod_hour_input = document.createElement("select");
// 		mod_hour_input.setAttribute("id", "mod_hour_input");
// 		for(var d = 0; d < 24; d++){
// 			var mod_hour_option = document.createElement("option");
// 			mod_hour_option.setAttribute("Value", d);
// 			mod_hour_option.text = d;
// 			mod_hour_input.appendChild(mod_hour_option);
// 		}
// 		var mod_minute_input = document.createElement("select");
// 		mod_minute_input.setAttribute("id", "mod_minute_input");
// 		for(var e = 0; e < 60; e++){
// 			var mod_minute_option = document.createElement("option");
// 			mod_minute_option.setAttribute("Value", e);
// 			mod_minute_option.text = e;
// 			mod_minute_input.appendChild(mod_minute_option);
// 		}
// 		var mod_token = document.createElement("input");
// 		mod_token.setAttribute("type", "hidden");
// 		mod_token.setAttribute("id", "mod_token");
// 		mod_token.setAttribute("value", "<?php echo $_SESSION['token'];?>");
// 		// ADD A FUNCTION TO THE BUTTON (SEND INFORMATION OF THE FORM TO PHP FILE)
// 		// populates div with initial add form
// 		mod_form.appendChild(mod_id);
// 		mod_form.appendChild(mod_name_input);
// 		mod_form.appendChild(mod_desc_input);
// 		mod_form.appendChild(document.createTextNode("Time: "));
// 		mod_form.appendChild(mod_hour_input);
// 		mod_form.appendChild(mod_minute_input);
// 		mod_form.appendChild(document.createTextNode("Date: "));
// 		mod_form.appendChild(mod_year_input);
// 		mod_form.appendChild(mod_month_input);
// 		mod_form.appendChild(mod_day_input);
// 		//mod_form.appendChild(mod_token);
// 		mod_form.appendChild(mod_submit);
// 		document.getElementById("calendar_manage").appendChild(mod_form);
// 		// creating a listener that waits for a month to be selected before enabling and populating day_input with options
// 		document.getElementById("mod_month_input").addEventListener("change", function(event){
// 			if(document.getElementById("mod_month_input").value != " "){
// 				document.getElementById("mod_day_input").remove();
// 				var mod_day_input = document.createElement("select");
// 				mod_day_input.setAttribute("id", "mod_day_input");
// 				// document.getElementById("mod_form").appendChild(mod_day_input);
// 				var mod_month = new Month(2016, mod_months.indexOf(document.getElementById("mod_month_input").value) - 1);
// 				var mod_weeks = mod_month.getWeeks();
// 				var mod_days_in_month = 0;
// 				for (var ix = 0; ix < mod_weeks.length; ix++){
// 					var mod_days = mod_weeks[ix].getDates();
// 					for(var dx = 0; dx < mod_days.length; dx++){
// 						if (mod_days[dx].getMonth() == mod_month.month){
// 							mod_days_in_month += 1;
// 							var mod_day_option = document.createElement("option");
// 							mod_day_option.setAttribute("value", mod_days_in_month);
// 							mod_day_option.text = mod_days_in_month;
// 							mod_day_input.appendChild(mod_day_option);
// 							//alert(add_days[dx].toISOString() + "  " + add_days_in_month);
// 						}
// 					}
// 				}
// 				document.getElementById("mod_form").appendChild(mod_day_input);

// 			}

// 		}, false);
// 		document.getElementById("mod_submit").addEventListener("click", modAjax, false);
// 	}

// 	function modAjax(event){
// 			var mod_event_id = document.getElementById("mod_id").value;
// 			var mod_event_name = document.getElementById("mod_name_input").value;
// 			var mod_event_description = document.getElementById("mod_desc_input").value;
// 			var mod_event_month = String(document.getElementById("mod_month_input").selectedIndex);
// 			var mod_event_year = document.getElementById("mod_year_input").value;
// 			var mod_event_day;
// 			if(mod_event_month == " "){
// 				mod_event_day == " ";
// 			}
// 			else{
// 				mod_event_day = String(document.getElementById("mod_day_input").selectedIndex + 1);
// 			}
// 			var mod_event_hour = String(document.getElementById("mod_hour_input").selectedIndex);
// 			var mod_event_minute = String(document.getElementById("mod_minute_input").selectedIndex);
// 			// var add_token = document.getElementById("add_token");
// 			var uri_string = "mod_event_id=" + encodeURIComponent(mod_event_id) + "&mod_event_name=" + encodeURIComponent(mod_event_name)
// 				+ "&mod_event_description=" + encodeURIComponent(mod_event_description) + "&mod_event_month=" + encodeURIComponent(mod_event_month)
// 				+ "&mod_event_year=" + encodeURIComponent(mod_event_year) +  "&mod_event_day=" + encodeURIComponent(mod_event_day) + "&mod_event_hour=" + encodeURIComponent(mod_event_hour) +
// 				"&mod_event_minute=" + encodeURIComponent(mod_event_minute);
// 			var mod_http = new XMLHttpRequest();
// 			mod_http.open("POST", "mod_event.php", true);
// 			mod_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
// 			mod_http.addEventListener("load", function(event){
// 				var mod_event_json = JSON.parse(event.target.responseText);
// 				alert(mod_event_json.message);
// 				if (mod_event_json.success){

// 				}
// 			}, false);
// 			mod_http.send(uri_string);
// 		}

function populateShare(login_json){
		var share_form = document.createElement("form");
		share_form.setAttribute("id", "share_form");
		share_form.appendChild(document.createElement("br"));
		share_form.appendChild(document.createElement("br"));
		share_form.appendChild(document.createTextNode("ADD OTHER USERS TO EVENT: "));
		var share_user = document.createElement("input");
		share_user.setAttribute("type", "text");
		share_user.setAttribute("id", "share_user");
		share_user.setAttribute("placeholder", "Other Username");
		var share_event = document.createElement("input");
		share_event.setAttribute("type", "text");
		share_event.setAttribute("id", "share_event");
		share_event.setAttribute("placeholder", "Event ID");
		var share_submit = document.createElement("input");
		share_submit.setAttribute("type", "button");
		share_submit.setAttribute("id", "share_submit");
		share_submit.setAttribute("value", "Share");
		var share_token = document.createElement("input");
		share_token.setAttribute("type", "hidden");
		share_token.setAttribute("id", "share_token");
		share_token.setAttribute("value", login_json.hidden_token);
		share_form.appendChild(share_event);
		share_form.appendChild(share_user);
		share_form.appendChild(share_submit);
		share_form.appendChild(share_token);
		document.getElementById("calendar_manage").appendChild(share_form);
		document.getElementById("share_submit").addEventListener("click", shareAjax, false);
		//viewEvents();
	}

function shareAjax(event){
	var raw_input = document.getElementById("share_event").value;
	if (isNaN(raw_input)){
		alert("Must input number for id");
	}
	else{
		var sharedevent = document.getElementById("share_event").value;
		var shareduser = document.getElementById("share_user").value;
		var share_token = document.getElementById("share_token");
		var uri_string = "sharedevent=" + encodeURIComponent(sharedevent) + "&shareduser=" + encodeURIComponent(shareduser) + "&share_token=" + encodeURIComponent(share_token);
		var mod_http = new XMLHttpRequest();
		mod_http.open("POST", "share_event.php", true);
		mod_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		mod_http.addEventListener("load", function(event){
			var share_event_json = JSON.parse(event.target.responseText);
			alert(share_event_json.message);
		}, false);
		mod_http.send(uri_string);
	}
}

function viewEvents(){
	var view_categories_input = document.createElement("select");
	view_categories_input.setAttribute("id", "view_categories_input");
	var mult_work_option = document.createElement("option");
	mult_work_option.setAttribute("value", "Work");
	mult_work_option.text = "Work";
	view_categories_input.appendChild(mult_work_option);
	var mult_school_option = document.createElement("option");
	mult_school_option.setAttribute("value", "School");
	mult_school_option.text = "School";
	view_categories_input.appendChild(mult_school_option);
	var mult_tasks_option = document.createElement("option");
	mult_tasks_option.setAttribute("value", "Tasks");
	mult_tasks_option.text = "Tasks";
	view_categories_input.appendChild(mult_tasks_option);
	var mult_meetings_option = document.createElement("option");
	mult_meetings_option.setAttribute("value", "Meetings");
	mult_meetings_option.text = "Meetings";
	view_categories_input.appendChild(mult_meetings_option);
	var view_multiple_p = document.createElement("p");
	view_multiple_p.setAttribute("id", "view_multiple_p");

	document.getElementById("events_multi_view").appendChild(view_categories_input);
	document.getElementById("events_multi_view").appendChild(view_multiple_p);

	document.getElementById("view_categories_input").addEventListener("change", function(event){
		var view_multiple_http = new XMLHttpRequest();
		view_multiple_http.open("POST", "view_event.php", true);
		view_multiple_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var uri_string = "category=" + encodeURIComponent(document.getElementById("view_categories_input").options[document.getElementById("view_categories_input").selectedIndex].value);
		view_multiple_http.addEventListener("load", function(event){
			var view_multiple_json = JSON.parse(event.target.responseText);
			var print_minute = String(view_multiple_json.minute).length == 1 ? String(view_multiple_json.minute) : "0" + String(view_multiple_json.minute);
			document.getElementById("view_multiple_p").innerHTML += "\n id: " + view_multiple_json.id + " event_name: " + view_multiple_json.event_name +
			" description: " + view_multiple_json.description + " category: " + view_multiple_json.category + " date: " +
			(view_multiple_json.month+1) + "/" + view_multiple_json.day + "/" + view_multiple_json.year + " time: " + view_multiple_json.hour + ":" + print_minute;
		}, false);
		view_multiple_http.send(uri_string);
	}, false);
}

function viewSpecificEvent(login_json){
	var view_event_text = document.createElement("input");
	view_event_text.setAttribute("type", "text");
	view_event_text.setAttribute("id", "view_event_text");
	view_event_text.setAttribute("placeholder", "Event ID");

	var view_event_button = document.createElement("input");
	view_event_button.setAttribute("type", "button");
	view_event_button.setAttribute("value", "View Event");
	view_event_button.setAttribute("id", "view_event_button");

	var view_token = document.createElement("input");
	view_token.setAttribute("type", "hidden");
	view_token.setAttribute("id", "view_token");
	view_token.setAttribute("value", login_json.hidden_token);

	var view_p = document.createElement("p");
	view_p.setAttribute("id", "view_p");
	document.getElementById("events_single_view").appendChild(document.createElement("br"));
	document.getElementById("events_single_view").appendChild(document.createElement("br"));
	document.getElementById("events_single_view").appendChild(document.createTextNode("VIEW EVENT: "));
	document.getElementById("events_single_view").appendChild(view_event_text);
	document.getElementById("events_single_view").appendChild(view_event_button);
	document.getElementById("events_single_view").appendChild(view_p);
	document.getElementById("events_single_view").appendChild(view_token);
	document.getElementById("view_event_button").addEventListener("click", function(event){
		var raw_input = document.getElementById("view_event_text").value;
		if (isNaN(raw_input)){
			alert("Must input number for id");
		}
		else{
			var view_event_id = String(document.getElementById("view_event_text").value);
			var view_token = document.getElementById("view_token").value;
			var uri_string = "view_event_id=" + encodeURIComponent(view_event_id) + "&view_token=" + encodeURIComponent(view_token);
			var view_http = new XMLHttpRequest();
			view_http.open("POST", "view_event.php", true);
			view_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			view_http.addEventListener("load", function(event){
				var view_event_json = JSON.parse(event.target.responseText);
				var print_minute = String(view_event_json.minute).length == 1 ? String(view_event_json.minute) : "0" + String(view_event_json.minute);
				document.getElementById("view_p").innerHTML = " id: " + view_event_json.id + " event_name: " + view_event_json.event_name +
				" description: " + view_event_json.description + " category: " + view_event_json.category + " date: " +
				(view_event_json.month+1) + "/" + view_event_json.day + "/" + view_event_json.year + " time: " + view_event_json.hour + ":" + print_minute;
			}, false);
			view_http.send(uri_string);
		}

	}, false);
}

function deleteUser(login_json){
	var del_user_button = document.createElement("input");
	del_user_button.setAttribute("type", "button");
	del_user_button.setAttribute("id", "del_user_button");
	del_user_button.setAttribute("value", "Delete User");
	document.getElementById("login").appendChild(del_user_button);
	document.getElementById("del_user_button").addEventListener("click", function(event){
		var mod_http = new XMLHttpRequest();
		mod_http.open("POST", "delete_user.php", true);
		mod_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		var uri_string = "";
		mod_http.addEventListener("load", function(event){
			var share_event_json = JSON.parse(event.target.responseText);
			alert(share_event_json.message);
		}, false);
		mod_http.send(uri_string);
		location.reload(true);
	});

}
