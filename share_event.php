<?php
header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
session_start();
if(((string)$_POST['sharedevent'] == "") or ((string)$_POST['shareduser'] == "")){
	if ($_SESSION['token'] !== $_POST['add_token']) {
		die("Request forgery detected.");
	}
					echo json_encode(array(
			"success" => false,
			"message" => "Enter all values"
			));
			exit;
}

$id = $_POST['sharedevent'];
$otheruser = $_POST['shareduser'];
$creator = "no";
$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');

if($mysqli->connect_errno) {
			echo json_encode(array(
			"success" => false,
			"message" => "Add Event Failed. Error: Connection Failed"
			));
			exit;
}
$getUserFromID = $mysqli->prepare("select username from user_event where event_id=(?)");
if(!$getUserFromID){
			echo json_encode(array(
			"success" => false,
			"message" => "Modify Event Failed. Error: Query Failed"
			));
			exit;
	}
$getUserFromID->bind_param('s', $id);
$getUserFromID -> execute();
$id_result = $getUserFromID -> get_result();
while($row = $id_result->fetch_assoc()){
	if($_SESSION['username'] == $row['username']){
		$sharemystory = $mysqli->prepare("insert into user_event (event_id, username, creator) values (?, ?, ?)");
		if(!$sharemystory){
			echo json_encode(array(
			"success" => false,
			"message" => "Modify Event Failed. Error: Query Failed"
			));
			exit;
		}

		$sharemystory->bind_param('sss', $id, $otheruser, $creator);
		$sharemystory->execute();
		$sharemystory->close();
		echo json_encode(array(
			"success" => true,
			"message" => "Story Shared"
		));
		exit;

	}
	echo json_encode(array(
		"success" => false,
		"message" => "Not your story"
	));
	exit;
}
echo json_encode(array(
		"success" => false,
		"message" => "Story cannot be shared. Not found in database"
	));
	exit;
?>