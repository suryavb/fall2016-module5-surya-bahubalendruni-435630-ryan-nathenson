<?php
header("Content-Type: application/json");
if(isset($_POST['view_event_id'])){
	ini_set("session.cookie_httponly", 1);
	session_start();

	$category = $_POST['category'];
	$username = $_SESSION['username'];
	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');

	if($mysqli->connect_errno) {
				echo json_encode(array(
				"success" => false,
				"message" => "Error: Connection Failed"
				));
				exit;
	}

	$view_userevent_query = $mysqli->prepare("select * from user_event where username=(?)");
	if(!$view_userevent_query){
		echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
	}
	$view_userevent_query-> bind_param('s', $username);
	$view_userevent_query->execute();

	$view_userevent_query_result = $view_userevent_query->get_result();
	// $usereventrow = $view_userevent_query_result->fetch_assoc();
	// if(is_null($usereventrow)){
	// 	echo json_encode(array(
	// 			"success" => false,
	// 			"message" => "User does not have any events!"
	// 			));
	// 			exit;
	// }
	while($usereventrow = $view_userevent_query_result->fetch_assoc()){
		$view_event_query = $mysqli->prepare("select * from events where id=(?) and category=(?)");
		if(!$view_event_query){
			echo json_encode(array(
					"success" => false,
					"message" => "Error: Query Prep Failed"
					));
					exit;
		}
		$view_event_query->bind_param('ss', $usereventrow['event_id'], $category);
		$view_event_query->execute();
		$view_event_query_result = $view_event_query->get_result();
		$eventrow = $view_event_query_result->fetch_assoc();
		echo json_encode(array(
			"success" => true,
			"id" => htmlspecialchars($eventrow['id']),
			"event_name" => htmlspecialchars($eventrow['event_name']),
			"description" => htmlspecialchars($eventrow['description']),
			"category" => htmlspecialchars($eventrow['category']),
			"year" =>htmlspecialchars($eventrow['year']),
			"month" => htmlspecialchars($eventrow['month']),
			"day" => htmlspecialchars($eventrow['day']),
			"hour" => htmlspecialchars($eventrow['hour']),
			"minute" => htmlspecialchars($eventrow['minute'])
		));
	}

	exit;
}
?>