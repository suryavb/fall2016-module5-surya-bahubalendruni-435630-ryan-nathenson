<?php
header("Content-Type: application/json");
if(isset($_POST['del_event_id'])){
	ini_set("session.cookie_httponly", 1);
	session_start();
	if ($_SESSION['token'] !== $_POST['del_token']) {
		die("Request forgery detected.");
	}

	$del_event = $_POST['del_event_id'];
	$username = $_SESSION['username'];
	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');

	if($mysqli->connect_errno) {
				echo json_encode(array(
				"success" => false,
				"message" => "Error: Connection Failed"
				));
				exit;
	}

	$del_event_query = $mysqli->prepare("select * from user_event where username=(?) and event_id=(?)");
	if(!$del_event_query){
		echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
	}
	$del_event_query->bind_param('ss', $username, $del_event);
	$del_event_query->execute();

	$del_event_query_result = $del_event_query->get_result();
	$row = $del_event_query_result->fetch_assoc();
	if(is_null($row)){
		echo json_encode(array(
				"success" => false,
				"message" => "User does not have an event with that id!"
				));
				exit;
	}
	if($row['creator'] == "yes"){
		$del_all_userevent_query = $mysqli->prepare("delete from user_event where event_id=(?)");
		if(!$del_all_userevent_query){
			echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
		}
		$del_all_userevent_query->bind_param('s', $del_event);
		$del_all_userevent_query->execute();
		$del_all_userevent_query->close();

		$del_all_event_query = $mysqli->prepare("delete from events where id=(?)");
		if(!$del_all_event_query){
			echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
		}
		$del_all_event_query->bind_param('s', $del_event);
		$del_all_event_query->execute();
		$del_all_event_query->close();
		echo json_encode(array(
				"success" => true,
				"message" => "Deleted event from calendar for all users"
				));
				exit;
	}
	else{
		$del_individual_query = $mysqli->prepare("delete from user_event where username=(?) and event_id=(?)");
		if(!$del_individual_query){
			echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
		}
		$del_individual_query->bind_param('ss', $username, $del_event);
		$del_individual_query->execute();
		$del_individual_query->close();
		echo json_encode(array(
				"success" => true,
				"message" => "Deleted event from your calendar"
				));
				exit;

	}


	// echo json_encode(array(
	// "success" => false,
	// "message" => $add_event_name . $add_event_description . $add_event_year . $add_event_month . $add_event_day . $_SESSION['username'] . $add_event_hour . $add_event_minute
	// ));
	// exit;
}
?>