<?php
	header("Content-Type: application/json");
	ini_set("session.cookie_httponly", 1);
	session_start();

	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');
	if($mysqli->connect_errno) {
				echo json_encode(array(
				"success" => false,
				"message" => "Add Event Failed. Error: Connection Failed"
				));
				exit;
	}

	$del_user_query = $mysqli->prepare("select * from user_event where username=(?)");
	if(!$del_user_query){
		echo json_encode(array(
				"success" => false,
				"message" => "Error: Query Prep Failed"
				));
				exit;
	}
	$del_user_query->bind_param('s', $_SESSION['username']);
	$del_user_query->execute();
	$del_user_query_result = $del_user_query->get_result();
	while($row = $del_user_query_result->fetch_assoc()){
		if($row['creator'] == "yes"){
			$deluserevent = $mysqli->prepare("delete from user_event where event_id = (?)");
			if(!$deluserevent){
				exit;
			}
			$deluserevent->bind_param('s', $row['event_id']);
			$deluserevent->execute();
			$deluserevent->close();
			$delevent = $mysqli -> prepare("delete from events where id = (?)");
			if(!$delevent){
				exit;
			}
			$delevent->bind_param('s', $row['event_id']);
			$delevent->execute();
			$delevent->close();
		}
		else{
			$del = $mysqli->prepare("delete from user_event where username = (?) and event_id = (?)");
			if(!del){
				exit;
			}
			$del->bind_param('ss', $_SESSION['username'], $row['event_id']);
			$del->execute();
			$del->close();
		}
	}

	$deluser = $mysqli->prepare("delete from users where username = (?)");
	if(!$deluser){
		exit;
	}
	$deluser->bind_param('s', $_SESSION['username']);
	$deluser->execute();
	$deluser->close();
	exit;
?>


