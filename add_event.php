<?php
header("Content-Type: application/json");
if(isset($_POST['add_event_name']) and isset($_POST['add_event_description']) and isset($_POST['add_event_month']) and isset($_POST['add_event_year']) and isset($_POST['add_event_day']) and isset($_POST['add_event_hour']) and isset($_POST['add_event_minute'])){
	ini_set("session.cookie_httponly", 1);
	session_start();
	if ($_SESSION['token'] !== $_POST['add_token']) {
		die("Request forgery detected.");
	}
	$categories = array(
		"Work",
		"School",
		"Tasks",
		"Meetings"
		);

	$add_event_name = (string) $_POST['add_event_name'];
	$add_event_description = (string) $_POST['add_event_description'];
	$add_event_category = $categories[(int)$_POST['add_event_category']];
	$add_event_hour = (int) $_POST['add_event_hour'];
	$add_event_minute = (int) $_POST['add_event_minute'];
	$add_event_month = (int) $_POST['add_event_month'];
	$add_event_year = (int) $_POST['add_event_year'];
	$add_event_day = (int) $_POST['add_event_day'];
	$create = "yes";
	// echo json_encode(array(
	// "success" => false,
	// "message" => $add_event_name . $add_event_description . $add_event_year . $add_event_month . $add_event_day . $_SESSION['username'] . $add_event_hour . $add_event_minute .$add_event_category
	// ));
	// exit;
	$mysqli = new mysqli('localhost', 'wustl_inst', 'wustl_pass', 'calendar');
	if($mysqli->connect_errno) {
				echo json_encode(array(
				"success" => false,
				"message" => "Add Event Failed. Error: Connection Failed"
				));
				exit;
	}
	$addevent = $mysqli->prepare("insert into events (event_name, description, category, year, month, day, hour, minute) values (?, ?, ?, ?, ?, ?, ?, ?)");
		if(!$addevent){
				echo json_encode(array(
				"success" => false,
				"message" => "Add Event Failed. Error: Query Failed"
				));
				exit;
		}
		$addevent->bind_param('ssssssss', $add_event_name, $add_event_description, $add_event_category,$add_event_year, $add_event_month, $add_event_day, $add_event_hour, $add_event_minute);
		$addevent->execute();
		$addevent->close();

	$get_event_id = $mysqli->prepare("select max(id) as max_id from events");
	if(!$get_event_id){
				echo json_encode(array(
				"success" => false,
				"message" => "Add Event Failed. Error: Query Failed"
				));
				exit;
		}
	$get_event_id -> execute();
	$id_result = $get_event_id -> get_result();
	while($row = $id_result->fetch_assoc()){
		$add_user_event = $mysqli->prepare("insert into user_event (event_id, username, creator) values (?, ?, ?)");
			if(!$add_user_event){
				echo json_encode(array(
				"success" => false,
				"message" => "Add Event Failed. Error: Query Failed"
				));
				exit;
			}
			$add_user_event->bind_param('sss', $row['max_id'], $_SESSION['username'], $create);
			$add_user_event->execute();
			$add_user_event->close();
				echo json_encode(array(
				"success" => true,
				"message" => "Event was successfully added"
				));
				exit;
	}


}
?>